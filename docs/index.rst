.. pylib documentation master file, created by
   sphinx-quickstart on Mon Jul 20 14:51:18 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pylib's documentation!
=================================

Contents:

.. toctree::
    :maxdepth: 2

    intro
    em

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

